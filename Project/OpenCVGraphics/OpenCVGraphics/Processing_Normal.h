#pragma once
#include "Processing_Base.h"

namespace OpenCVGraphics {

	class Processing_Normal :
		public Processing_Base
	{
	public:
		int Update();//アップデート関数
		int Initialize();//初期化関数
	};

}