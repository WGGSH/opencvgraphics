// OpenCVGraphics.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"


int main()
{
	OpenCVGraphics::Graphic *graphic = new OpenCVGraphics::Graphic();
	
	graphic->Initialize();

	if (graphic->Update()==-1)
	{
		printf("プロセスエラー\n");
		return -1;
	}

	delete graphic;

    return 0;
}

