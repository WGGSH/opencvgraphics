#pragma once
#include "Processing_Base.h"

namespace OpenCVGraphics {
	class Processing_HSVEdit :
		public Processing_Base
	{
		const int HUE_MIN = 0;
		const int HUE_MAX = 180;
		const int SAT_MIN = -180;
		const int SAT_MAX = 180;
		const int BRI_MIN = -180;
		const int BRI_MAX = 180;
	private:
		int hue;
		int sat;
		int bri;
	public:
		int Update();//アップデート関数
		int Initialize();//初期化関数
	};

}