#pragma once

namespace OpenCVGraphics 
{
	//定数
	const int CAMERA_MAX = 9;

	//Cameraクラス
	class Camera
	{
	private:
		cv::VideoCapture capture;//カメラデバイス
		cv::Mat frame;//カメラから得た画像データ

		int Open();//カメラの解放

	public:
		Camera();//コンストラクタ
		~Camera();//デストラクタ

		int Update();//毎フレーム毎の更新
		int Initialize();//初期化

		cv::Mat Get_Capture();//カメラの映像を取得する
	};

}