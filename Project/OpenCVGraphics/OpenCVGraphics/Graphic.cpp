#include "stdafx.h"

using namespace OpenCVGraphics;

#include "Processing_Normal.h"
#include "Processing_FaceDetect.h"
#include "Processing_HSVEdit.h"
#include "Processing_Transparency.h"

//コンストラクタ
Graphic::Graphic()
{
	this->camera = new Camera();
}

//デストラクタ
Graphic::~Graphic()
{
	delete this->camera;
	this->process.release();
	this->mat_draw.release();
}

//初期化関数
//返値
//成功:0
//失敗:-1
int Graphic::Initialize() {
	//キー状態の初期化
	this->key = GRAPHIC_NO_KEY;

	std::vector<int> initialize_log_list;
	initialize_log_list.clear();
	initialize_log_list.push_back(this->camera->Initialize());

	for (auto log : initialize_log_list) 
	{
		if (log == -1)
		{
			printf("初期化失敗 終了します\n");
			return -1;
		}
	}
	
	printf("初期化完了");
	return 0;
}

//アップデート関数
int Graphic::Update() 
{
	printf("処理開始\n");
	printf("[S]:画面保存\n[Q]:終了\n[R]:再起動\n");
	
	//プロセスの設定
	//this->process = std::make_unique<Processing_Normal>();
	this->Process_Set();

	//プロセスの初期化
	this->process->Initialize();

	while (1) {
		//カメラの更新
		this->camera->Update();

		//プロセスにカメラ画像を渡す
		this->process->Set_Mat(this->camera->Get_Capture());

		//プロセスの更新
		this->process->Update();

		//プロセスが生成したMatの取得
		this->mat_draw = this->process->Get_Processed_Mat();

		//描画
		cv::namedWindow("Window", 0);
		cv::imshow("Window", this->mat_draw);

		//キー判定
		this->key = this->Key_Input();
		switch (this->key)
		{
		case GRAPHIC_QUIT:
			printf("Q:終了\n");
			return 0;
			break;
		case GRAPHIC_REBOOT:
			this->Process_Set();
			break;
		case GRAPHIC_SAVE:
			break;
		case GRAPHIC_NO_KEY://入力なし
			break;
		default:
			printf("キー入力エラー\n");
			return -1;
			break;
		}
	}
	return 0;
}

GRAPHIC_KEY Graphic::Key_Input() 
{
	//キーの取得
	int key=cv::waitKey(1);
	switch (key) {
	case 113:
		return GRAPHIC_QUIT;
		break;
	case 114:
		return GRAPHIC_REBOOT;
		break;
	case 115:
		return GRAPHIC_SAVE;
		break;
	default:
		return GRAPHIC_NO_KEY;
		break;
	}
	return GRAPHIC_NO_KEY;
}

Camera& Graphic::Get_Camera() {
	return *this->camera;
}

int Graphic::Process_Set() {
	if (this->process != nullptr) {
		this->process.release();
	}

	
	while (1) {
		printf("起動するプロセスを選択\n");
		printf("[0]:テスト用\n");
		printf("[1]:顔認識\n");
		printf("[2]:HSV加工\n");
		printf("[3]:透明化\n");
		int key = -1;
		scanf_s("%d", &key);
		switch (key) {
		case 0:
			this->process = std::make_unique<Processing_Normal>();
			//this->process->Initialize();
			return 0;
			break;
		case 1:
			this->process = std::make_unique<Processing_FaceDetect>();
			//this->process->Initialize();
			return 1;
			break;
		case 2:
			this->process = std::make_unique<Processing_HSVEdit>();
			//this->process->Initialize();
			return 1;
			break;
		case 3:
			this->process = std::make_unique<Processing_Transparency>();
			//this->process->Initialize();
			return -1;
			break;
		default:
			printf("入力エラー");
			continue;
			break;
		}
	}

	return 0;
}