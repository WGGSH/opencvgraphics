#include "stdafx.h"
#include "Processing_Normal.h"

using namespace OpenCVGraphics;

//初期化関数
int Processing_Normal::Initialize() {
	return 0;
}

//アップデート関数
//受け取った画像を返すだけ
int Processing_Normal::Update() {
	this->mat_processed = this->mat_base.clone();
	return 0;
}