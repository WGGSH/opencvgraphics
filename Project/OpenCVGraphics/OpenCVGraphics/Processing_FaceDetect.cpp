#include "stdafx.h"
#include "Processing_FaceDetect.h"

using namespace OpenCVGraphics;

//初期化関数
int Processing_FaceDetect::Initialize()
{
	//分類器の読み込み
	this->cascade_file.load("..\\DataFile\\Processing_FaceDetect\\haarcascade_frontalface_alt.xml");
	if (this->cascade_file.empty() == true)
	{
		return -1;//読み込み失敗
	}
	return 0;
}

std::vector<cv::Rect> faces;

//アップデート関数
//受け取った画像を返すだけ
int Processing_FaceDetect::Update() 
{
	//this->faces.clear();

	cv::Mat gray;
	cv::cvtColor(this->mat_base, gray, CV_BGR2GRAY);

	double scale = 0.1;

	cv::Size min_size = cv::Size(this->mat_base.cols*scale, this->mat_base.rows*scale);
	this->cascade_file.detectMultiScale(
		gray, faces, 
		1.1, 3, 0, min_size);

	//for (auto rect : this->faces) {
		/*cv::rectangle(
			this->mat_base,
			cv::Point(rect.x, rect.y),
			cv::Point(rect.x + rect.width, rect.y + rect.height),
			cv::Scalar(0, 200, 0), 3, CV_AA);*/
	/*for (int i = 0; i < this->faces.size(); i++) {
		cv::rectangle(
			this->mat_base,
			cv::Point(this->faces[i].x, this->faces[i].y),
			cv::Point(this->faces[i].x + this->faces[i].width,
				this->faces[i].y + this->faces[i].height),
			cv::Scalar(0, 200, 0), 5);
	}*/
	//}

	this->mat_processed = this->mat_base.clone();
	return 0;
}