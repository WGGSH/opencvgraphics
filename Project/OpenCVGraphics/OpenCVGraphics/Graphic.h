#pragma once
#include "Camera.h"
#include "Processing_Base.h"


namespace OpenCVGraphics {
	extern class Camera;
	extern class Processing_Base;
	//キー入力定数
	enum GRAPHIC_KEY 
	{
		GRAPHIC_QUIT,//Q:終了
		GRAPHIC_REBOOT,//R:プロセスの再起動
		GRAPHIC_SAVE,//S:画面保存
		GRAPHIC_NO_KEY,//キー入力なし/未定義の入力
	};

	class Graphic
	{
		Camera *camera;//カメラクラス
		std::unique_ptr<Processing_Base> process;//画像処理クラス
		GRAPHIC_KEY key;//キー入力値
		cv::Mat mat_draw;//描画用のMat

		GRAPHIC_KEY Key_Input();//キーを受け取る関数

		int Process_Set();
	public:
		Graphic();//コンストラクタ
		~Graphic();//デストラクタ

		int Update();//アップデート関数
		int Initialize();//初期化関数

		Camera& Get_Camera();//カメラを渡す
	};

	
}