#include "stdafx.h"
#include "Processing_HSVEdit.h"


using namespace OpenCVGraphics;

int Processing_HSVEdit::Initialize() {
	this->hue = 0;
	this->sat = 0;
	this->bri = 0;
	cv::namedWindow("Trackbar", CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);
	cv::createTrackbar("hue", "Trackbar", &this->hue, this->HUE_MAX*2);
	cv::createTrackbar("sat", "Trackbar", &this->sat, 360);
	cv::createTrackbar("bri", "Trackbar", &this->bri, 360);

	return 0;
}

int Processing_HSVEdit::Update() {
	cv::Mat mat;
	cv::cvtColor(this->mat_base, mat, CV_BGR2HSV);

	//mat.channels[0] += 60;
	cv::Mat channels[3];
	cv::split(mat, channels);

	channels[0] += this->hue;
	channels[1] += this->sat-180;
	channels[2] += this->bri - 180;

	cv::merge(channels, 3, mat);

	cv::cvtColor(mat, this->mat_processed, CV_HSV2BGR);

	return 0;
}