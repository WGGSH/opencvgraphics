#include "stdafx.h"
#include "Processing_Base.h"

using namespace OpenCVGraphics;

//コンストラクタ
Processing_Base::Processing_Base() 
{
}

//デストラクタ
Processing_Base::~Processing_Base() {
	this->mat_base.release();
	this->mat_processed.release();
}

//加工前のMatを受け取る関数
void Processing_Base::Set_Mat(cv::Mat mat) {
	this->mat_base = mat.clone();
}

//加工後のMatを渡す関数
cv::Mat Processing_Base::Get_Processed_Mat() {
	return this->mat_processed;
}