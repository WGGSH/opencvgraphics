#include "stdafx.h"
#include "Camera.h"

using namespace OpenCVGraphics;

//コンストラクタ
Camera::Camera()
{	
	this->frame =  cv::Mat();
}

//デストラクタ
Camera::~Camera()
{
	this->capture.release();
	this->frame.release();
}

//初期化関数
int Camera::Initialize() 
{
	return this->Open();
}

//アップデート関数
int Camera::Update() 
{
	//カメラから得た画像を渡す
	this->capture >> this->frame;

	return 0;
}

//カメラを起動する関数
//返値
//成功:解放した番号
//失敗:-1
int Camera::Open() {
	std::vector<int> openable_camera_list;
	openable_camera_list.clear();

	for (int i = 0; i < CAMERA_MAX; i++)
	{
		this->capture = cv::VideoCapture(i);
		if (this->capture.isOpened())
		{
			//読み込み成功時
			//printf("カメラ初期化処理:%d番の起動成功\n", i);
			//return i;
			openable_camera_list.push_back(i);
		}
	}

	if (openable_camera_list.size() == 0)
	{
		//読み込みに失敗
		printf("カメラ初期化処理:失敗");
		return -1;
	}

	printf("カメラ初期化処理:開放するカメラ番号の選択\n");
	for (auto number : openable_camera_list) 
	{
		//起動可能なカメラを列挙する
		printf("[%d]", number);
	}
	printf("\n");
	
	int open_number = -1;

	//起動できるカメラが1つの場合
	//それを起動
	if (openable_camera_list.size() == 1) {
		this->capture = cv::VideoCapture(*openable_camera_list.begin());
		printf("カメラ初期化処理:%d盤のカメラを起動成功\n", *openable_camera_list.begin());
		return *openable_camera_list.begin();
	}

	//2つ以上ある場合
	//適切な入力が行われるまでループ
	//-1で起動処理終了
	do {
		scanf_s("%d", &open_number);
		for (auto number : openable_camera_list)
		{
			if (open_number == number)
			{
				//カメラを起動して,起動したカメラの番号を返す
				this->capture = cv::VideoCapture(open_number);
				printf("カメラ初期化処理:%d番のカメラを起動成功\n",open_number);
				return open_number;
			}
		}
		printf("カメラ初期化処理:入力エラー\n");
	} while (open_number!=-1);

	//読み込みに失敗
	printf("カメラ初期化処理:失敗");
	return -1;
	
}

cv::Mat Camera::Get_Capture() 
{
	return this->frame;
}