#pragma once
#include "Processing_Base.h"

namespace OpenCVGraphics 
{
	class Processing_FaceDetect :
		public Processing_Base
	{
	private:
		cv::CascadeClassifier cascade_file;//分類器
		std::vector<cv::Rect> faces;//検出した矩形リスト
		
	public:
		int Update();//アップデート関数
		int Initialize();//初期化関数
	};


}