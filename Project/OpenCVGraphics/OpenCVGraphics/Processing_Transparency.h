#pragma once
#include "Processing_Base.h"

namespace OpenCVGraphics {
	class Processing_Transparency :
		public Processing_Base
	{
	private:
		cv::Mat mat_back;//wipฬMat
		int state;//๓ิ(0:wiw่O/1:wiw่ใ)
		int hue;//F
		int saturation;//สx
		int brightness;//Px

		int hue_range;//Fฬออ
		int saturation_range;//สxฬออ
		int brightness_range;

		int Transparency_rate;//ง฿ฆ

		void State_GetBackGround();//๓ิ0:wiๆพา@
		void State_ShowTransparency();//๓ิ1:งพ

		void CreateTrackBar();//gbNo[ฬถฌ
		void ShowSampleColor();//งพปท้ฉ{Fฬ\ฆ
	public:

		int Initialize();
		int Update();
	};

}