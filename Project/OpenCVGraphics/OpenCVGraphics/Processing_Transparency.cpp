#include "stdafx.h"
#include "Processing_Transparency.h"

using namespace OpenCVGraphics;

int Processing_Transparency::Initialize() 
{

	this->hue = 120;
	this->hue_range = 31;
	this->saturation = 114;
	this->saturation_range = 91;
	this->brightness = 197;
	this->brightness_range = 77;

	this->Transparency_rate = 50;

	this->state = 0;
	printf("Press Enter Key\n");
	return 0;
}

int Processing_Transparency::Update() 
{
	switch (this->state) {
	case 0://背景指定前
		this->State_GetBackGround();
		this->mat_processed = this->mat_base.clone();
		break;
	case 1://背景指定後
		this->ShowSampleColor();
		this->State_ShowTransparency();

		//this->mat_back = this->mat_base.clone();
		//cv::cvtColor(this->mat_back, this->mat_back, CV_BGR2HSV);
		break;
	default:
		break;
	}


	//this->mat_processed = this->mat_base.clone();
	return 0;
}

//状態1
void Processing_Transparency::State_GetBackGround() 
{
	//キー入力
	int key = -1;

	//キー入力を受け付ける
	key = cv::waitKey(1);

	//Enterキーが押されたら
	if (key == 116)
	{
		//背景画像を記憶
		this->mat_back = this->mat_base.clone();
		//HSB変換
		//cv::cvtColor(this->mat_back, this->mat_back, CV_BGR2HSV);

		//状態を変更する
		this->state = 1;

		//トラックバーの生成
		this->CreateTrackBar();
	}
}

//状態2
//透明化を行う
void Processing_Transparency::State_ShowTransparency() 
{
	//透明後のMat
	cv::Mat mat_trans = this->mat_base.clone();
	//HSVに変換
	cv::cvtColor(mat_trans, mat_trans, CV_BGR2HSV);
	
	//画像サイズの取得
	int width = mat_trans.cols;
	int height = mat_trans.rows;

	//抽出範囲の設定
	
	//色相
	int hue_min = (this->hue - this->hue_range+180)%180;
	int hue_max = (this->hue + this->hue_range+180)%180;
	
	//彩度
	int saturation_min = this->saturation - this->saturation_range;
	int saturation_max = this->saturation + this->saturation_range;

	//輝度
	int brightness_min = this->brightness - this->brightness_range;
	int brightness_max = this->brightness + this->brightness_range;

	//調査対象のピクセルの色情報
	int hue_target;
	int saturation_target;
	int brightness_target;

	//画像の全ての座標を調べる
	for (int y = 0; y < height; y++){
		for (int x = 0; x < width; x++){

			//探索座標の色を調べる

			//重い
			
			/*hue_target        = mat_trans.at<cv::Vec3b>(y, x)[0];
			saturation_target  = mat_trans.at<cv::Vec3b>(y, x)[1];
			brightness_target = mat_trans.at<cv::Vec3b>(y, x)[2];*/


			hue_target = mat_trans.data[(y*width + x)*3 + 0];
			saturation_target = mat_trans.data[(y*width + x)*3 + 1];
			brightness_target = mat_trans.data[(y*width + x)*3 + 2];
			
			//変換を行うかの判定フラグ
			bool flag = true;

			/*
			//彩度を判定
			if (satrution_target >= saturation_min &&
				satrution_target <= saturation_max)
			{
				//輝度を判定
				if (brightness >= brightness_min &&
					brightness <= brightness_max)
				{
					//色相を判定
					if (hue_target >= hue_min &&
						hue_target <= hue_max)
					{
						//変換する
						flag = true;
					}

					if (hue_target <= hue_min ||
						hue_target >= hue_max)
					{
						//変換する
						flag = true;
					}

				}
			}*/

			//彩度判定
			if (!(saturation_target >= saturation_min &&
				saturation_target <= saturation_max))
			{
				flag = false;
			}

			//輝度判定
			if (!(brightness_target >= brightness_min&&
				brightness_target <= brightness_max))
			{
				flag = false;
			}

			//色相判定
			if (hue_min <= hue_max)
			{

				if (!(hue_target >= hue_min&&
					hue_target <= hue_max))
				{
					flag = false;
				}
			}
			else
			{
				if (!(hue_target > hue_max ||
					hue_target < hue_min))
				{
					flag = false;
				}
			}

			
			if (flag == true)
			{
				//変換処理
				//mat_trans.at<cv::Vec3b>(y, x) = cv::Vec3b(0, 255, 255);
				
				//mat_trans.at<cv::Vec3b>(y, x) = this->mat_back.at<cv::Vec3b>(y, x);
				/*mat_trans.at<cv::Vec3b>(y, x)[0] = this->mat_back.at<cv::Vec3b>(y, x)[0] + mat_trans.at<cv::Vec3b>(y, x)[0];
				mat_trans.at<cv::Vec3b>(y, x)[1] = this->mat_back.at<cv::Vec3b>(y, x)[1] + mat_trans.at<cv::Vec3b>(y, x)[1];
				mat_trans.at<cv::Vec3b>(y, x)[2] = this->mat_back.at<cv::Vec3b>(y, x)[2] + mat_trans.at<cv::Vec3b>(y, x)[2];*/

				for (int i = 0; i < 3; i++)
				{
					//重い
					/*mat_base.at<cv::Vec3b>(y, x)[i] =
						this->mat_back.at<cv::Vec3b>(y, x)[i] / 100.0*this->Transparency_rate +
						mat_base.at<cv::Vec3b>(y, x)[i] / 100.0*(100 - this->Transparency_rate);*/

					//高速版(エラー処理を行わない)
					//画像サイズやチャンネル数が分かっているので使用可能
					this->mat_base.data[(y*width + x)*3 + i] =
					//this->mat_base.at<cv::Vec3b>(y,x)[i]=
						this->mat_back.data[(y*width + x)*3 + i] / 100.0*this->Transparency_rate +
						this->mat_base.data[(y*width + x)*3 + i] / 100.0*(100.0 - this->Transparency_rate);
				}

			}

		}
	}

	//HSV画像をBGR画像に戻す
	//cv::cvtColor(mat_trans, mat_trans, CV_HSV2BGR);

	//画像を渡す
	//this->mat_processed = mat_trans.clone();
	this->mat_processed = this->mat_base.clone();
	

}

//トラックバーの生成
void Processing_Transparency::CreateTrackBar() 
{

	std::string window_name_color = "指定色";
	std::string window_name_range = "色範囲";
	std::string window_name_transparency = "透過率";

	//ここから抽出する色の基準点
	//トラックバーを配置するウィンドウの生成
	cv::namedWindow(window_name_color, CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);

	cv::createTrackbar("色相", window_name_color, &this->hue, 180);
	cv::createTrackbar("彩度", window_name_color, &this->saturation, 255);
	cv::createTrackbar("輝度", window_name_color, &this->brightness, 255);

	//ここから抽出する色の範囲
	//トラックバーを配置するウィンドウの生成
	cv::namedWindow(window_name_range, CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);

	cv::createTrackbar("色相", window_name_range, &this->hue_range, 90);
	cv::createTrackbar("彩度", window_name_range, &this->saturation_range, 255);
	cv::createTrackbar("輝度", window_name_range, &this->brightness_range, 255);

	//ここから透過率の設定
	cv::namedWindow(window_name_transparency, CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);

	cv::createTrackbar("透過率", window_name_transparency, &this->Transparency_rate,100);
}

//見本色の表示
void Processing_Transparency::ShowSampleColor()
{
	//サンプル画像の生成
	cv::Mat mat_sample(cv::Size(150, 150), CV_8UC3, cv::Scalar(0, 0, 0));
	//HSVに変換
	cv::cvtColor(mat_sample, mat_sample, CV_BGR2HSV);
	//指定色で塗りつぶす
	mat_sample = cv::Scalar(this->hue, this->saturation, this->brightness);
	//RGBに戻す
	cv::cvtColor(mat_sample, mat_sample, CV_HSV2BGR);

	cv::imshow("色見本", mat_sample);
}