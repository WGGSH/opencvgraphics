#pragma once


namespace OpenCVGraphics {
	extern class Graphic;
	class Processing_Base
	{
	public:
		cv::Mat mat_base;//受け取った加工前のMat
		cv::Mat mat_processed;//加工後のMat
		Processing_Base();//コンストラクタ
		~Processing_Base();//デストラクタ

		virtual int Initialize() = 0;//初期化関数
		virtual int Update() = 0;//アップデート関数

		void Set_Mat(cv::Mat mat);//加工用のMatをセットする
		cv::Mat Get_Processed_Mat();//加工後のMatを返却する
	};

}