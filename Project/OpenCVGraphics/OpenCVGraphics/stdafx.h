// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

//警告の非表示
#pragma warning (disable:4819)
#pragma warning (disable:4091)

// TODO: プログラムに必要な追加ヘッダーをここで参照してください
#include <vector>
#include <memory>

#include <opencv2\opencv.hpp>
#include "Graphic.h"